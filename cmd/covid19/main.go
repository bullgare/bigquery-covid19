// data is available at https://console.cloud.google.com/bigquery?project=manifest-access-247608&folder=&organizationId=&j=bq:EU:bquxjob_19bd9ec5_1716d478e50&page=queryresults
package main

import (
	"context"
	"fmt"
	"log"

	"cloud.google.com/go/bigquery"
	"cloud.google.com/go/civil"
	giterator "google.golang.org/api/iterator"
)

const projectID = "bullgare-bigquery"

type CountryDataByDate struct {
	Name      string             `bigquery:"country_region"`
	Date      civil.Date         `bigquery:"date"`
	Confirmed bigquery.NullInt64 `bigquery:"confirmed"`
	Deaths    bigquery.NullInt64 `bigquery:"deaths"`
	Recovered bigquery.NullInt64 `bigquery:"recovered"`
	Active    bigquery.NullInt64 `bigquery:"active"`
}

func main() {
	ctx := context.Background()
	client, err := bigquery.NewClient(ctx, projectID)
	if err != nil {
		log.Fatalf("failed to create client: %v", err)
	}

	runQuery(ctx, client)
}

func runQuery(ctx context.Context, client *bigquery.Client) {
	q := client.Query(`
SELECT 
	country_region, 
	date, 
	SUM(confirmed) AS confirmed, 
	SUM(deaths) AS deaths, 
	SUM(recovered) AS recovered, 
	SUM(active) AS active
FROM bigquery-public-data.covid19_jhu_csse_eu.summary
WHERE date BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 30 DAY) AND CURRENT_DATE()
GROUP BY country_region, date
ORDER BY date, confirmed DESC;
`)
	iterator, err := q.Read(ctx)
	if err != nil {
		log.Fatalf("failed to read: %v", err)
	}

	countriesData := make([]CountryDataByDate, 3000)

	for {
		var countryDataByDate CountryDataByDate
		err := iterator.Next(&countryDataByDate)
		if err == giterator.Done {
			break
		}
		if err != nil {
			log.Fatalf("failed to iterate results: %v", err)
		}

		countriesData = append(countriesData, countryDataByDate)
	}

	for _, countryData := range countriesData {
		fmt.Printf("Country of %s for %s: %v total, %v deaths, %v active, %v recovered\n",
			countryData.Name,
			countryData.Date.String(),
			countryData.Confirmed,
			countryData.Deaths,
			countryData.Active,
			countryData.Recovered,
		)
	}
	// or
	// for {
	// 	var values []bigquery.Value
	// 	err := iterator.Next(&values)
	// 	if err == giterator.Done {
	// 		break
	// 	}
	// 	if err != nil {
	// 		log.Fatalf("failed to iterate results: %v", err)
	// 	}
	//
	// 	fmt.Println(values)
	// }
}
