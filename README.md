## Run

### Before run

1. You need to set `GOOGLE_APPLICATION_CREDENTIALS`
according to this article - https://cloud.google.com/docs/authentication/getting-started.
2. Ask me to add you to the project `bullgare-bigquery`.

## Useful links

[Dataset: Johns Hopkins University Coronavirus COVID-19 Global Cases, by country](https://console.cloud.google.com/marketplace/details/johnshopkins/covid19_jhu_global_cases?filter=solution-type:dataset&filter=category:covid19&id=430e16bb-bd19-42dd-bb7a-d38386a9edf5)

[Admin panel](https://console.cloud.google.com/iam-admin/serviceaccounts?project=bullgare-bigquery)

[Web UI console](https://console.cloud.google.com/bigquery?project=manifest-access-247608&folder=&organizationId=&p=bigquery-public-data&d=covid19_jhu_csse_eu&t=summary&page=table)

[Bigquery functions reference](https://cloud.google.com/bigquery/docs/reference/standard-sql/functions-and-operators)

[Golang library reference](https://pkg.go.dev/cloud.google.com/go/bigquery?tab=doc)