module gitlab.com/bullgare/bigquery-convid19

go 1.13

require (
	cloud.google.com/go v0.56.0
	cloud.google.com/go/bigquery v1.6.0
	google.golang.org/api v0.21.0
)
